<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();


/*************************INICIO PÀGINA INICIAL***************/
Route::get('/', 'PageController@index')->name('page.index');

Route::get('/pesquisa', 'PageController@pesquisa')->name('page.pesquisa');
Route::get('/desenvolvimento', 'PageController@desenvolvimento')->name('page.desenvolvimento');
Route::get('/prototipo', 'PageController@prototipo')->name('page.prototipo');
Route::get('/mercado', 'PageController@mercado')->name('page.mercado');

Route::get('/banner/{id}/show', 'PageController@show')->name('page.show');

Route::get('/home', 'HomeController@index')->name('home.index');
/*************************FIM PÀGINA INICIAL***************/


/*************************INICIO ADMIN CATEGORIA***************/
Route::resource('categoria', 'CategoryController');
/*************************FIM ADMIN CATEGORIA***************/

/*************************INICIO ADMIN PROJETO***************/
Route::resource('projeto', 'ProjectController');
/*************************FIM ADMIN PROJETO***************/

/*************************INICIO ADMIN NIT***************/
Route::resource('nit', 'NitController');

/*************************FIM ADMIN NIT***************/

