<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class)->create([
            'name' => 'Miqueias',
            'email' => 'miqueias@email.com',
            'role' => 'admin',
            'password' => bcrypt('123mudar')
        ]);
        factory(App\User::class, 2)->create();
    }
}
