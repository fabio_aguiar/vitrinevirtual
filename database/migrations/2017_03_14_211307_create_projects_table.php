<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('categoria_id')->unsigned()->index()->nullable();
            $table->foreign('categoria_id')->references('id')->on('categories')->onDelete('cascade');
            $table->integer('nit_id')->unsigned()->index()->nullable();
            $table->foreign('nit_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('nome')->nullable();
            $table->string('problema',500)->nullable();
            $table->string('solucao',500)->nullable();
            $table->string('beneficio',500)->nullable();
            $table->string('oportunidade',500)->nullable();
            $table->string('contato',500)->nullable();
            $table->integer('estagio')->nullable();
            $table->string('diferencial',500)->nullable();
            $table->string('mercado',500)->nullable();
            $table->string('imagem')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('projects');
    }
}
