<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = ['categoria_id','nit_id','nome','problema','solucao','beneficio','oportunidade','contato','estagio','diferencial','mercado','imagem'];

    public function category()
    {
        return $this->belongsTo(Category::class,'categoria_id');
    }

    public function user()
    {
        return $this->belongsTo(Category::class,'nit_id');
    }

}


