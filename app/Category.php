<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['categoria'];

    public function projects()
    {
        return $this->hasMany(Project::class,'categoria_id');
    }
}
