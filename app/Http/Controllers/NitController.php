<?php

namespace App\Http\Controllers;

use App\Http\Requests\Nit;
use App\Http\Requests\NitUpdate;
use App\User;
use Illuminate\Http\Request;

class NitController extends Controller
{

    /**
     * @var User
     */
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    public function index()
    {
        $nits = $this->getUser()->paginate(5);

        return view('nits.index',compact('nits'));
    }


    public function store(Nit $request)
    {
        $request->merge(['password' => Hash::make($request->password)]);

        $this->getUser()->create($request->all());

        return redirect()->route('nit.index');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $nit = $this->getUser()->find($id);
        return view('nits.edit', compact('nit'));

    }

    public function update(NitUpdate $request, $id)
    {
        $this->getUser()->find($id)->update($request->all());
        return redirect()->route('nit.index');
    }

    public function destroy($id)
    {
        $this->getUser()->find($id)->delete();
        return redirect()->route('nit.index');

    }
}
