<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\ProjectRequest;
use App\Http\Requests\ProjectUpdateRequest;
use App\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProjectController extends Controller
{


    /**
     * @var Project
     */
    private $project;
    /**
     * @var Category
     */
    private $category;

    public function __construct(Project $project, Category $category)
    {
        $this->project = $project;
        $this->category = $category;
    }

    /**
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }


    public function index()
    {
        $nit_id = Auth::user()->id;

        $projects = $this->getProject()->where('nit_id',$nit_id)->paginate(5);

        $categories = $this->getCategory()->all()->pluck('categoria','id');

        return view('projects.index',compact('projects','categories'));
    }


    public function store(ProjectRequest $request)
    {
        $hash = rand(100,10000);

        $data_arquivo					= date('Ymd');
        $nome_arquivo 					= $data_arquivo.'_'.$hash.'.png';

        $project = new Project(array(
            'categoria_id' => $request->get('categoria_id'),
            'nit_id'  => $request->get('nit_id'),
            'nome'  => $request->get('nome'),
            'problema'  => $request->get('problema'),
            'solucao'  => $request->get('solucao'),
            'beneficio'  => $request->get('beneficio'),
            'oportunidade'  => $request->get('oportunidade'),
            'contato'  => $request->get('contato'),
            'estagio'  => $request->get('estagio'),
            'diferencial'  => $request->get('diferencial'),
            'mercado'  => $request->get('mercado'),
            'imagem'  => $nome_arquivo

        ));

        $project->save();

        $request->file('imagem')->move(public_path().'/uploads',$project->imagem);

        return redirect()->route('projeto.index')->with('message','Projeto cadastrado com sucesso!');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $project = $this->getProject()->find($id);
        $categories = $this->getCategory()->all()->pluck('categoria','id');
        return view('projects.edit', compact('project','categories'));

    }

    public function update(ProjectUpdateRequest $request, $id)
    {
        $this->getProject()->find($id)->update($request->all());
        return redirect()->route('projeto.index');
    }

    public function destroy($id)
    {
        $this->getProject()->find($id)->delete();
        return redirect()->route('projeto.index');

    }
}
