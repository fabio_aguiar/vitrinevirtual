<?php

namespace App\Http\Controllers;

use App\Category;
use App\Project;
use Illuminate\Http\Request;

class PageController extends Controller
{

    /**
     * @var Category
     */
    private $category;
    /**
     * @var Project
     */
    private $project;

    public function __construct(Category $category, Project $project)
    {

        $this->category = $category;
        $this->project = $project;
    }

    /**
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }

    public function index()
    {
        $projects = $this->getProject()->all();

        $categories = $this->getCategory()->all();

        return view('pages.index',compact('projects','categories'));
    }

    public function show($id)
    {
        $project = $this->getProject()->find($id);
        return view('pages.show', compact('project'));

    }

    public function pesquisa()
    {
        $projects = $this->getProject()->where('estagio', '=',1)->get();

        $categories = $this->getCategory()->all();

        return view('pages.index',compact('projects','categories'));
    }

    public function desenvolvimento()
    {
        $projects = $this->getProject()->where('estagio', '=',2)->get();

        $categories = $this->getCategory()->all();

        return view('pages.index',compact('projects','categories'));
    }

    public function prototipo()
    {
        $projects = $this->getProject()->where('estagio', '=',3)->get();

        $categories = $this->getCategory()->all();

        return view('pages.index',compact('projects','categories'));
    }

    public function mercado()
    {
        $projects = $this->getProject()->where('estagio', '=',4)->get();

        $categories = $this->getCategory()->all();

        return view('pages.index',compact('projects','categories'));
    }




}
