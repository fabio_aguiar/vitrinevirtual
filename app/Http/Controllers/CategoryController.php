<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\CategoryRequest;
use App\Http\Requests\CategoryUpdateRequest;


class CategoryController extends Controller
{

    /**
     * @var Category
     */
    private $category;

    public function __construct(Category $category)
    {

        $this->category = $category;
    }

    /**
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }



    public function index()
    {
        $categories = $this->getCategory()->paginate(5);

        return view('categories.index',compact('categories'));
    }


    public function store(CategoryRequest $request)
    {
        $this->getCategory()->create($request->all());

        return redirect()->route('categoria.index');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $category = $this->getCategory()->find($id);
        return view('categories.edit', compact('category'));

    }

    public function update(CategoryUpdateRequest $request, $id)
    {
        $this->getCategory()->find($id)->update($request->all());
        return redirect()->route('categoria.index');
    }

    public function destroy($id)
    {
        $this->getCategory()->find($id)->delete();
        return redirect()->route('categoria.index');

    }


}
