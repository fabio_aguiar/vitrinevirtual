@extends('layouts.base-index')

@section('title', 'Totem de projetos CESA')

@section('content')




        <div class="col-md-10">
            <h1>
                <div class="div_titulo_principal_banner">
                    {{$project->nome}}
                </div>
            </h1>
        </div>

        <div class="col-md-2" align="right">
                <div class="row">
                    <img src="{{ asset('img/logo_redenit.png')}}" />
                </div>
                <div id="menu_topo">
                    <a href="{{route('page.index')}}" class="btn btn-lg btn-success"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Voltar</a>
                </div>
        </div>

        <div class="col-md-12">
            <?php
            switch ($project->estagio)
            {
                case 1:
                    echo '<button class="btn btn-info tab_vinte"> Este projeto está na fase de PESQUISA';
                    break;
                case 2:
                    echo '<button class="btn btn-success tab_vinte"> Este projeto está na fase de DESENVOLVIMENTO';
                    break;
                case 3:
                    echo '<button class="btn btn-warning tab_vinte"> Este projeto está na fase de PROTÓTIPO';
                    break;
                case 4:
                    echo '<button class="btn btn-danger tab_vinte"> Este projeto está na fase de MERCADO';
                    break;
            }
            ?>
        </div>







    <div class="div_cem">
        <div class="div_row_container">
            <div class="div_box_banner">
                <div class="div_titulo_banner">PROBLEMA</div>
                <div class="div_texto_banner">
                    <p>
                        {{$project->problema}}
                    </p>
                </div>
            </div>
            <div class="div_box_banner">
                <img class="img_banner" src="{{ asset('uploads').'/'.$project->imagem}}"/>
            </div>
            <div class="div_box_banner">
                <div class="div_titulo_banner">SOLUÇÃO</div>
                <div class="div_texto_banner">
                    <p>
                        {{$project->solucao}}
                    </p>
                </div>
            </div>
            <div class="div_box_banner">
                <div class="div_titulo_banner">BENEFÍCIO</div>
                <div class="div_texto_banner">
                    <p>
                        {{$project->beneficio}}
                    </p>
                </div>
            </div>
            <div class="div_box_banner">
                <div class="div_titulo_banner">OPORTUNIDADE</div>
                <div class="div_texto_banner">
                    <p>
                        {{$project->oportunidade}}
                    </p>
                </div>
            </div>

            <div class="div_box_banner">
                <div class="div_titulo_banner">DIFERENCIAL</div>
                <div class="div_texto_banner">
                    <p>
                        {{$project->diferencial}}
                    </p>
                </div>
            </div>
            <div class="div_box_banner">
                <div class="div_titulo_banner">POTENCIAL DE MERCADO</div>
                <div class="div_texto_banner">
                    <p>
                        {{$project->mercado}}
                    </p>
                </div>
            </div>
            <div class="div_box_banner">
                <div class="div_titulo_banner">CONTATO</div>
                <div class="div_texto_banner">
                    <p>
                        {{$project->contato}}
                    </p>
                </div>
            </div>

        </div>
    </div>

@endsection
