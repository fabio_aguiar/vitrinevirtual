@extends('layouts.base-index')

@section('title', 'Totem de projetos CESA')

@section('sidebar')
    @parent
@endsection

@section('content')

@include('layouts.home-base')

    <br>
    @if($projects)
        @foreach($projects as $project)
        <div class="col-md-4">
            <div class="div_box_container">
                <div class="div_titulo_container">{{substr($project->nome, 0, 22).'...'}}</div>
                <div class="div_img_box">
                    <a href="{{route('page.show',[$project->id])}}"><img src="{{ asset('uploads').'/'.$project->imagem }}" width="330" height="220" class="img_box" /></a>
                </div>
            </div>
        </div>
        @endforeach
    @else
        <h3>Nenhum portifólio cadastrado!</h3>
    @endif



@endsection




