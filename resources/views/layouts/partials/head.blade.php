<head>
    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>@yield('title')</title>

    <!-- Bootstrap -->

    <link rel="stylesheet" type="text/css" href=" {{ asset('assets/bootstrap/css/bootstrap.css') }}" />
    <link rel="stylesheet" type="text/css" href=" {{ asset('assets/custom/css/app.css') }}" />


    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <script src="{{ asset('plugin/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('plugin/fontawesome/fontawesome.js') }}"></script>
    <script src="{{ asset('assets/custom/js/app.js') }}"></script>


</head>
