<html>

@include('layouts.partials.head')
@show

<body>

    <div>
        @yield('content')
    </div>

</body>
</html>