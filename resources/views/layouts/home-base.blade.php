<!------------------------------------------------------ PARTE SUPERIOR DA PÁGINA INICIAL--------------------------------------->
<div class="container-fluid">
    <div class="row">
        <div class="col-md-8">
            <div >
                <h1>Portifólio Tecnológico da Redenit - CE </h1>
                <h3>Estágio da tecnologia:</h3>
                <a href="{{route('page.pesquisa')}}" class="btn btn-primary" id="pesquisa"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> PESQUISA</a>
                <a href="{{route('page.desenvolvimento')}}" class="btn btn-success" id="desenvolvimento"><span class="glyphicon glyphicon-wrench" aria-hidden="true"></span> DESENVOLVIMENTO</a>
                <a href="{{route('page.prototipo')}}" class="btn btn-warning" id="prototipo"><span class="glyphicon glyphicon-picture" aria-hidden="true"></span> PROTÓTIPO</a>
                <a href="{{route('page.mercado')}}" class="btn btn-danger" id="mercado"><span class="glyphicon glyphicon-check" aria-hidden="true"></span> MERCADO</a>
            </div>
        </div>


        <div class="col-md-4" align="right">
            <div class="row">
                <img src="{{ asset('img/logo_redenit.png')}}"/>
            </div>
            <br>
            <div id="row">
                <strong>Categorias </strong>
                <select class="drop_down" id="cat_topo" name="cat_topo">
                    <option value="0">-- TODAS --</option>
                    @if($categories)
                        @foreach($categories as $category)
                            <option value="{{$category->id}}">{{$category->categoria}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>

    </div>
</div>
<!------------------------------------------------------ FIM PARTE SUPERIOR DA PÁGINA INICIAL--------------------------------------->