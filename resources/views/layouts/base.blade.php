<html>

@include('layouts.partials.head')
@show

<body>

    <nav class="navbar navbar-default navbar-static-top">
        @yield('nav')
    </nav>

    <div>
        @yield('content')
    </div>

</body>
</html>