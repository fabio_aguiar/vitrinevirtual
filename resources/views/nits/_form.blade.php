<div>
    <div id="error"></div>
    {{ csrf_field() }}
    <div class="form-group">
        <label for="name" class="control-label">Nome</label>
        <div>
            {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nome', 'id' => 'name']) !!}
        </div>
    </div>

    <div class="form-group">
        <label for="email" class="control-label">Email</label>
        <div>
            {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Email', 'id' => 'email']) !!}
        </div>
    </div>

    <div class="form-group">
        <label for="password" class="control-label">Senha</label>
        <div>
            {!!Form::password('password', ['class' => 'form-control','placeholder' => 'Senha'])!!}
        </div>
    </div>

    <div class="form-group">
        <label for="password-confirm" class="control-label">Confirme</label>
        <div>
            {!!Form::password('password-confirm', ['class' => 'form-control','placeholder' => 'Confime'])!!}
        </div>
    </div>

</div>