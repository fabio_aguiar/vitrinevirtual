<div>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div id="error"></div>

    {{ Form::hidden('nit_id',Auth::user()->id) }}

    <div class="form-group">
        <label for="name" class="control-label">Categoria</label>
        <div>
            {{ Form::select('categoria_id',$categories,null,['class'=>'form-control','id'=>'categoria_id','placeholder'=>'Selecione uma Categoria']) }}
        </div>
    </div>

    <div class="form-group">
        <label for="name" class="control-label">Nome</label>
        <div>
            {!! Form::text('nome', null, ['class' => 'form-control', 'placeholder' => 'Nome', 'id' => 'nome']) !!}
        </div>
    </div>

    <div class="form-group">
        <label for="name" class="control-label">Problema</label>
        <div>
            {!! Form::text('problema', null, ['class' => 'form-control', 'placeholder' => 'Categoria', 'id' => 'categoria']) !!}
        </div>
    </div>

    <div class="form-group">
        <label for="name" class="control-label">Solucao</label>
        <div>
            {!! Form::text('solucao', null, ['class' => 'form-control', 'placeholder' => 'Solucao', 'id' => 'solucao']) !!}
        </div>
    </div>

    <div class="form-group">
        <label for="name" class="control-label">Beneficio</label>
        <div>
            {!! Form::text('beneficio', null, ['class' => 'form-control', 'placeholder' => 'Beneficio', 'id' => 'beneficio']) !!}
        </div>
    </div>

    <div class="form-group">
        <label for="name" class="control-label">Oportunidade</label>
        <div>
            {!! Form::text('oportunidade', null, ['class' => 'form-control', 'placeholder' => 'Oportunidade', 'id' => 'oportunidade']) !!}
        </div>
    </div>

    <div class="form-group">
        <label for="name" class="control-label">Contato</label>
        <div>
            {!! Form::text('contato', null, ['class' => 'form-control', 'placeholder' => 'Contato', 'id' => 'contato']) !!}
        </div>
    </div>

    <div class="form-group">
        <label for="name" class="control-label">Estagio</label>
        <div>
            {!! Form::select('estagio',['1'=>'Pesquisa','2'=>'Desenvolvimento','3'=>'Protótipo','4'=>'Mercado'], null,['class' => 'form-control', 'placeholder' => 'Estagio', 'id' => 'estagio']) !!}
        </div>
    </div>

    <div class="form-group">
        <label for="name" class="control-label">Diferencial</label>
        <div>
            {!! Form::text('diferencial', null, ['class' => 'form-control', 'placeholder' => 'Diferencial', 'id' => 'diferencial']) !!}
        </div>
    </div>

    <div class="form-group">
        <label for="name" class="control-label">Mercado</label>
        <div>
            {!! Form::text('mercado', null, ['class' => 'form-control', 'placeholder' => 'Mercado', 'id' => 'mercado']) !!}
        </div>
    </div>

    <div class="form-group">
        <label for="name" class="control-label">Imagem</label>
        <div>
            {!! Form::file('imagem',null,['class' => 'form-control', 'placeholder' => 'Imagem', 'id' => 'imagem']) !!}
        </div>
    </div>
</div>