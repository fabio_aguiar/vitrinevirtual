@extends('layouts.base')

@section('nav')
    @include('layouts.partials.menu')
@endsection

@section('content')
<div class="container">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Listagem de Projetos
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home.index')}}"> Inicial</a></li>
            <li class="active">Projetos</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Projetos cadastradas</h3>
                <div>
                    @if($errors->any())
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $error)
                                <p>{{$error}}</p>
                            @endforeach
                        </div>
                    @endif
                </div>
                <div class="box-tools">
                    <a href="#" class="btn btn-info pull-right" data-toggle="modal" data-target="#modalcreate"><i class="fa fa-plus"> </i> Novo Projeto</a>
                </div>
            </div>
            <div class="box-body">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Nome</th>
                        <th>Criado em</th>
                        <th>Atualizado em</th>
                        <th style="width: 5px;" colspan="2" class="text-center">Ações</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if($projects)
                        @foreach($projects as $project)
                            <tr>
                                <td>{{ $project->id }}</td>
                                <td>{{ $project->nome }}</td>
                                <td>{{ $project->created_at->format('d/m/Y') }}</td>
                                <td>{{ $project->updated_at->format('d/m/Y') }}</td>
                                <td  style="width: 5px;">
                                    <a href='{{ route('projeto.edit', ['id'=>$project->id])}}' class="btn btn-action btn-info"><i class="fa fa-edit"></i></a>
                                </td>
                                <td  style="width: 5px;">
                                    <a href="#modal_delete-{{$project->id}}" class="btn btn-danger" data-toggle="modal"><i class="fa fa-trash-o"></i></a>

                                    {{-- modal delete --}}
                                    <div class="modal fade" id="modal_delete-{{ $project->id}}" data-backdrop='false'>
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h4 class="modal-title">Deletar Projeto</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <h3 class="text-center text-danger" > Tem certeza que deseja deletar esta projeto?</h3>
                                                    <div class="row">
                                                        <div class="col-md-6 ">  <button type="button" class="btn btn-default btn-block" data-dismiss="modal">Não</button> </div>
                                                        <div class="col-md-6 ">
                                                            {!! Form::open(['route'=>['projeto.destroy', $project->id], 'method' => 'delete']) !!}
                                                            {!! Form::submit('Sim',['class'=>'btn  btn-danger btn-block']) !!}
                                                            {!! Form::close() !!}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5">Não há dados para exibir</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                {{ $projects->render() }}
            </div>
            <!-- /.box-footer-->
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->


<!-- Modal Cadastro de Projeto -->
<div class="modal fade" id="modalcreate" tabindex="-1" role="dialog" aria-labelledby="modalcreateLabel" data-backdrop="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modalcreateLabel">Cadastro de Projeto</h4>
            </div>
            <div class="modal-body">
                {!! Form::open(array('route' => 'projeto.store','class' => 'form','novalidate' => 'novalidate','files' => true)) !!}
                @include('projects._form')

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal"> <i class="fa fa-close"></i> Cancelar</button>
                <button type="submit" class="btn btn-primary"> <i class="fa fa-floppy-o"></i> Salvar</button>
                {!! Form::close() !!}

            </div>
        </div>
    </div>
</div>
</div>
@endsection



