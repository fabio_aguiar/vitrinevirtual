@extends('layouts.base')

@section('nav')
    @include('layouts.partials.menu')
@endsection

@section('content')
<div class="container">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Edição de Projetos
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home.index')}}"> Inicial</a></li>
            <li><a href="{{ route('projeto.index')}}"> Projetos</a></li>
            <li>Edição</li>
        </ol>
    </section>
    <div>
        @if($errors->any())
            <div class="alert alert-danger">
                @foreach($errors->all() as $error)
                    <p>{{$error}}</p>
                @endforeach
            </div>
        @endif
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    {!! Form::model($project,[
                        'route'=>['projeto.update', $project->id],
                        'method' => 'put',
                        'files' => true
                        ])
                        !!}
                    @include('projects._form')
                    <div class="content">
                        <a href="{{ route('projeto.index') }}" class="btn btn-default"><i class="fa fa-arrow-left"></i> Voltar </a>
                        <button type="submit" class="btn btn-primary pull-right"> <i class="fa fa-floppy-o"></i> Salvar</button>
                    </div>
                    {!! Form::close() !!}

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
</div>
@endsection
