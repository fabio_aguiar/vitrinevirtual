<div>
    <div id="error"></div>
    <div class="form-group">
        <label for="name" class="control-label">Nome</label>
        <div>
            {!! Form::text('categoria', null, ['class' => 'form-control', 'placeholder' => 'Categoria', 'id' => 'categoria']) !!}
        </div>
    </div>
</div>